package vn.toancauxanh.gg.model;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.image.Image;
import org.zkoss.zul.Window;

import vn.toancauxanh.model.Model;
import vn.toancauxanh.model.NhanVien;

@Entity
@Table(name = "House")
public class House extends Model<House>{
	private String name = "";
	
	private String description = "";
	
	private NhanVien manager;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne
	public NhanVien getManager() {
		return manager;
	}

	public void setManager(NhanVien manager) {
		this.manager = manager;
	}
	
	@Command
	public void saveHouse(@BindingParam("list") final Object listObject,
			@BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException{
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, listObject, attr);
	}
	
	@Transient
	public AbstractValidator getValidatorHouse() {
		return new AbstractValidator() {
			@Override
			public void validate(final  ValidationContext ctx) {
				/*if (getImageContent() == null) {
					addInvalidMessage(ctx, "error",
							"Bạn chưa chọn hình ảnh cho banner.");
				}
				Date fromDate = getNgayBatDau();
				Date toDate = getNgayHetHan();
				if (fromDate != null && toDate != null) {
					if (fromDate.compareTo(toDate) > 0) {
						addInvalidMessage(ctx, "lblErr",
								"Ngày hết hạn phải lớn hơn hoặc bằng ngày bắt đầu.");
					}
				}*/
			}
		};
	}
	
}
