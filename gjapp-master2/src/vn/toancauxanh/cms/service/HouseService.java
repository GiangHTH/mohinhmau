package vn.toancauxanh.cms.service;

import org.apache.commons.collections.MapUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.House;
import vn.toancauxanh.gg.model.QHouse;
import vn.toancauxanh.service.BasicService;

public class HouseService extends BasicService<House>{
	private String img = "/backend/assets/img/edit.png";
	private String hoverImg = "/backend/assets/img/edit_hover.png";
	/*private String strUpdate = "Thứ tự";*/
	private boolean update = true;
	private boolean updateThanhCong = true;
	
	public JPAQuery<House> getTargetQuery() {
		String paramImage = MapUtils.getString(argDeco(),Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"),"");
		
		System.out.println(paramImage);
		System.out.println(trangThai);
		
		JPAQuery<House> q = find(House.class)
				.where(QHouse.house.trangThai.ne(core().TT_DA_XOA));
		if (paramImage != null && !paramImage.isEmpty()) {
			String tukhoa = "%" + paramImage + "%";
			q.where(QHouse.house.name.like(tukhoa)
				.or(QHouse.house.description.like(tukhoa)));
		}
		if (!trangThai.isEmpty()) {
			q.where(QHouse.house.trangThai.eq(trangThai));
		}
		
		q.orderBy(QHouse.house.id.asc());
		return q;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getHoverImg() {
		return hoverImg;
	}

	public void setHoverImg(String hoverImg) {
		this.hoverImg = hoverImg;
	}

	/*public String getStrUpdate() {
		return strUpdate;
	}

	public void setStrUpdate(String strUpdate) {
		this.strUpdate = strUpdate;
	}*/

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public boolean isUpdateThanhCong() {
		return updateThanhCong;
	}

	public void setUpdateThanhCong(boolean updateThanhCong) {
		this.updateThanhCong = updateThanhCong;
	}
	
	
}
